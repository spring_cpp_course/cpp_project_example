#!/bin/bash
set +x

BOOST_URL=https://boostorg.jfrog.io/artifactory/main/release/1.81.0/source/boost_1_81_0.tar.gz
DEPS_FOLDER=$(pwd)/deps


function get_packet_manager() #функция определения пакетного менеджера
{
    declare -A osInfo; #объявить массив osInfo
    osInfo["redhat-release"]=yum #присвоить значение массиву
    osInfo["arch-release"]=pacman
    osInfo["gentoo-release"]=emerge
    osInfo["SuSE-release"]=zypper
    osInfo["debian_version"]=apt-get
    osInfo["alpine-release"]=apk

    for f in ${!osInfo[@]} #пробежаться по массиву
    do      
        if command -v ${osInfo[$f]} >/dev/null; then #если команда сработает, то 
            echo ${osInfo[$f]} #вывести ее
        fi
    done
}

function install_required_packages() #установка необходимых библиотек на Linux/MacOS
{
    PACKET_MANAGER=$(get_packet_manager)
    REQUIRED_PROGS=(wget autoconf make cmake automake gcc g++ git libtool libcurl-devel openssl-devel libuuid-devel)

    echo "Installing next packages: ${REQUIRED_PROGS[@]}"
    sudo $PACKET_MANAGER install ${REQUIRED_PROGS[@]} -y
}

function install_boost()
{
    local current_dir=$PWD

    mkdir $DEPS_FOLDER #создать папку deps

    wget $BOOST_URL #скачать boost по ссылке (переменная BOOST_URL)
    tar -xf boost_1_81_0.tar.gz #распаковать архив
    cd boost_1_81_0

    ./bootstrap.sh --prefix=${DEPS_FOLDER} --with-libraries=thread,program_options,chrono && ./b2 install \
    && cd $current_dir && rm -rf boost_1_81_0 

}

install_boost
