add_library(eigen_lib INTERFACE)
target_include_directories(eigen_lib INTERFACE ./)
target_link_libraries(eigen_lib INTERFACE Eigen3::Eigen)

add_library(additional_libs INTERFACE)
target_include_directories(additional_libs INTERFACE ${PROJECT_SOURCE_DIR}/include)
