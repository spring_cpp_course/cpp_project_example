
#-----BEGIN OF Sources List-----
#-----Common sources-----------
set(SOURCES_GEOMETIRC ${SOURCES_GEOMETIRC} geometric_mean/geometric_mean.cpp)

#build lib
add_library(solver_lib ${SOURCES_GEOMETIRC})
target_include_directories(solver_lib PUBLIC ./ ${PROJECT_SOURCE_DIR}/include)
target_link_libraries(solver_lib PUBLIC eigen_lib)


#demo application
add_executable (solver_runner main.cpp)
target_include_directories(solver_runner PUBLIC ./)
target_link_libraries(solver_runner PUBLIC solver_lib additional_libs)
