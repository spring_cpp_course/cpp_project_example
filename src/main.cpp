#include <geometric_mean/geometric_mean.hpp>
#include <Eigen/Dense>
#include <iostream>

int main() {
    Eigen::VectorXd X(3);
    X << 5, 4, 4;

    Eigen::VectorXd Y(3);
    Y << 1, 2, 3;

    Eigen::VectorXd result = getGeometricMean(X, Y);

    std::cout << "Result is: " << result << std::endl;
}