#pragma once

#include <Eigen/Dense>
#include <utility>


struct ScalingProduct {
    // the geometric mean itself
    Eigen::MatrixXd geometric_mean;

    struct ByProduct {
        // L from last X = LLT lower triangular Cholesky decomposition
        Eigen::MatrixXd x_lower_triangular;
        // R from last S = UUT upper triangular Cholesky decomposition
        Eigen::MatrixXd s_upper_triangular;
        // the minimum eigenvalue of the product of X and S
        double min_eigenvalue = 0;
    } by_product;
};

/// *************************************
/// Геометрическое среднее между двумя матрицами
/// @param X матрица прямого пространства
/// @param S матрица двойственного пространства
/// @return среднее значение
/// *************************************
ScalingProduct getGeometricMean(const Eigen::MatrixXd& X, const Eigen::MatrixXd& S);

/// *************************************
/// Геометрическое среднее между векторами
/// @param X вектор прямого пространства
/// @param S Матрица двойственного пространства
/// @return среднее значение
/// *************************************
Eigen::VectorXd getGeometricMean(const Eigen::VectorXd& X, const Eigen::VectorXd& S);