#include "geometric_mean.hpp"

#include <Eigen/Cholesky>


static double Sqrt(double x) // the functor we want to apply
{
    return std::sqrt(x);
}

static double Rsqrt(double x) // the functor we want to apply
{
    return 1.0 / std::sqrt(x);
}

ScalingProduct getGeometricMean(const Eigen::MatrixXd& X, const Eigen::MatrixXd& S) {
    assert(X.size() == S.size());

    // lower triangular Cholesky decomposition: X = LLT
    Eigen::MatrixXd lower_triangle = X.llt().matrixL();
    // upper triangular Cholesky decomposition: S = UUT
    Eigen::MatrixXd S_rotated = S.transpose().colwise().reverse().transpose().colwise().reverse().llt().matrixL();
    Eigen::MatrixXd upper_triangle = S_rotated.transpose().colwise().reverse().transpose().colwise().reverse();
    // R^T * L view
    Eigen::MatrixXd to_singularity = upper_triangle.transpose() * lower_triangle;

    Eigen::JacobiSVD<Eigen::MatrixXd> svd(to_singularity, Eigen::ComputeFullV);
    const auto& singular_values = svd.singularValues();
    double last_singular_value = *(--singular_values.end());
    auto rsquared_D = singular_values.unaryExpr(&Rsqrt).asDiagonal();

    // G = LVD^(-1/2) view
    auto G = lower_triangle * svd.matrixV() * rsquared_D;

    return {G * G.transpose(),{std::move(lower_triangle), std::move(upper_triangle),
             last_singular_value * last_singular_value}};
}

Eigen::VectorXd getGeometricMean(const Eigen::VectorXd& X, const Eigen::VectorXd& S)
{
    return (X.array()/S.array()).unaryExpr(&Sqrt);
}