# Solver

# Building
## Dependency
Зависимости находятся в папке deps/ и являются submodules:
- Eigen
- google_tests

Команда для подгрузки submodules:
```
git submodule update --init --recursive
```
## Library building
Библиотека собирается в build/src/solver_lib.a с помощью следующих команд:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) solver_lib
```

# Структура проекта
.
├── assembly - скрипты для сборки
│   ├── build_library.sh
│   └── README_example.md
├── build - папка, в которую собирается проект
│   ├── bin
│   ├── buildtests.sh
│   ├── check.sh
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   ├── cmake_install.cmake
│   ├── CTestCustom.cmake
│   ├── debug.sh
│   ├── deps
│   ├── include
│   ├── lib
│   ├── Makefile
│   ├── release.sh
│   ├── src
│   └── test
├── CMakeLists.txt - главный CMake-файл
├── deps - здесь содержаться зависимости (third-party библиотеки)
│   ├── CMakeLists.txt
│   ├── eigen
│   └── googletest
├── include - заголовочные файлы
│   ├── CMakeLists.txt
│   ├── json.hpp
│   ├── json_wrapper.hpp
│   └── rapidcsv.h
├── README.md - описание проекта
├── scripts - скрипты
│   └── install_boost.sh
├── src - основной код
│   ├── CMakeLists.txt
│   ├── geometric_mean
│   └── main.cpp
└── test - тесты
    ├── CMakeLists.txt
    └── unit_test