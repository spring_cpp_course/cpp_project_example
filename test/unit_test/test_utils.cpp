#include "test_utils.hpp"

#include <iostream>



Eigen::MatrixXcd TestUtils::matrix_pow(const Eigen::MatrixXd& matrix, const double& exp)
{
        // A^n = P * D^n * P^-1, where P - matrix of eigen vectors, D - matrix of eigen values
        Eigen::EigenSolver<Eigen::MatrixXd> es(matrix);
        Eigen::MatrixXcd P = es.eigenvectors();
        Eigen::MatrixXcd D = es.eigenvalues();
        Eigen::MatrixXcd P_inv = P.inverse();

        Eigen::MatrixXcd D_powed = Eigen::pow(D.array(), exp);

        return P * D_powed.asDiagonal() * P_inv;
}