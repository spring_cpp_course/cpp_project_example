#pragma once

#include <Eigen/Dense>

#include <random>

class SDP_Point_Min;
class SDP_Point_Max;
class SDP_AuxPoint;

namespace TestUtils {
    template <bool is_integral, typename T> struct uniform_distribution_selector;

    template <typename T> struct uniform_distribution_selector<true, T>
    {
    using type = typename std::uniform_int_distribution<T>;
    };

    template <typename T> struct uniform_distribution_selector<false, T>
    {
    using type = typename std::uniform_real_distribution<T>;
    };

    /// *************************************
    /// Целочисленный или дробный андомизатор от @param min до @param max
    /// @return число
    /// *************************************
    template <typename T>
    T get_random(int min, int max) {
        using uniform_distribution_type = typename uniform_distribution_selector<std::is_integral<T>::value, T>::type;
        std::random_device rd; 
        std::mt19937 gen(rd());
        uniform_distribution_type dis(min, max);
        return dis(gen);
    }

    /// *************************************
    /// Матричное возведение в степень с помощью поиска собственных чисел
    /// @param matrix матрица, которую возводят в степень
    /// @param exp экспонента степени
    /// @return комплексная матрица
    /// *************************************
    Eigen::MatrixXcd matrix_pow(const Eigen::MatrixXd& matrix, const double& exp);

    /// *************************************
    /// Квадратный корень из матрицы, такой что A^1/2 * A^1/2 = A
    /// @param matrix матрица из которой извлекают квадратный корень
    /// @return комплексная матрица
    /// *************************************
    inline Eigen::MatrixXcd matrix_sqrt(const Eigen::MatrixXd& matrix) 
    {
        return matrix_pow(matrix, 1.0/2.0);
    }

    /// *************************************
    /// Обратный квадратный корень из матрицы, такой что A^1/2 * A^-1/2 = E
    /// @param matrix матрица из которой извлекают обратный квадратный корень
    /// @return комплексная матрица
    /// *************************************
    inline Eigen::MatrixXcd matrix_rsqrt(const Eigen::MatrixXd& matrix) 
    {
        return matrix_pow(matrix, -1.0/2.0);
    }

}