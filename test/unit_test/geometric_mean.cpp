#include "geometric_mean/geometric_mean.hpp"
#include "test_utils.hpp"
#include <Eigen/Dense>
#include <gtest/gtest.h>

using namespace Eigen;

TEST(TestGeometricMean, MatrixPower) {
    MatrixXd X(2,2);
    X << 5, 4, 4, 5;

    MatrixXd result_matrix(2,2);
    result_matrix << 2, 1, 1, 2;

    MatrixXd X_sqrt = TestUtils::matrix_pow(X, 1.0/2.0).real();
    ASSERT_TRUE(result_matrix.isApprox(X_sqrt));
}

TEST(TestGeometricMean, MatrixSqrt) {
    MatrixXd X = MatrixXd::Random(25, 25).cwiseAbs();
    auto X_sqrt = TestUtils::matrix_sqrt(X);
    auto X_compute = X_sqrt * X_sqrt;

    ASSERT_TRUE(X.isApprox(X_compute));
}

TEST(TestGeometricMean, MatrixRSqrt) {
    MatrixXd X = MatrixXd::Random(40, 40);
    auto X_rsqrt = TestUtils::matrix_rsqrt(X);
    auto X_sqrt = TestUtils::matrix_sqrt(X);
    auto X_compute = X_rsqrt * X_sqrt;

    MatrixXd ones = MatrixXd::Identity(40, 40);
    ASSERT_TRUE(ones.isApprox(X_compute.real()));
}

TEST(TestGeometricMean, SmallRandomProblem) {
    MatrixXd X = MatrixXd::Random(1,1).cwiseAbs();
    MatrixXd S = MatrixXd::Random(1,1).cwiseAbs();

    auto W = getGeometricMean(X, S);
    double w_eval = std::sqrt(*X.data() / *S.data());
    EXPECT_FLOAT_EQ(*W.geometric_mean.data(), w_eval);
}

TEST(TestGeometricMean, VectorGeoMin) {
    size_t n = 10;
    VectorXd X = VectorXd::Random(n).cwiseAbs();
    VectorXd S = VectorXd::Random(n).cwiseAbs();

    auto W = getGeometricMean(X, S);
    for(size_t i = 0; i < n; i++) 
    {
        double value = std::sqrt(X[i]/S[i]);
        EXPECT_FLOAT_EQ(W[i], value);
    }
    double w_eval = std::sqrt(*X.data() / *S.data());
    EXPECT_FLOAT_EQ(*W.data(), w_eval);
}

TEST(TestGeometricMean, HugeRandomProblem) {
    MatrixXd X = MatrixXd::Random(4, 4);
    X = X*X.transpose();
    MatrixXd S = X.inverse();

    auto W = getGeometricMean(X, S);

    ASSERT_TRUE(W.geometric_mean.isApprox(X));
} 

TEST(TestGeometricMean, SizeAssertionChecker) {
    MatrixXd X = MatrixXd::Random(2,2);
    MatrixXd S = MatrixXd::Random(3,2);

    EXPECT_EXIT({
            auto W = getGeometricMean(X, S);
        }, ::testing::KilledBySignal(SIGABRT), ".*");
}