#!/bin/bash

CURRENT_DIR=$(pwd)
PROJECT_DIR="$(dirname "$CURRENT_DIR")"
BUILD_DIR="$PROJECT_DIR/build"

function createDir() {
    if [[ -d "$1" ]]
    then
        echo "$1 already exists."
    else
        mkdir $1
    fi
}

function build_library() {
    echo "Creating build dir..."
    createDir $1;
    cd $1;
    echo "Running cmake in release..."
    cmake -DCMAKE_BUILD_TYPE=Release $2;
    echo "Building library...."
    make -j4 solver_lib;
}

build_library $BUILD_DIR $PROJECT_DIR